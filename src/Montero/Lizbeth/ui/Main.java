package Montero.Lizbeth.ui;
/**
 * Clase Main, la cual es creada en el en la package o capa ui y aquí es en donde se interactua con el usuario.
 * @atour Lizbeth Montero Ramírez.
 * @version 1.0.1
 * @since 1.0.0
 */
import Montero.Lizbeth.tl.Controller;

import java.io.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();


    public static void main(String[] args) throws IOException {
        crearMenu();
    }// FIN DE MAIN

    /**
     * metodo en donde se le pide al usuario que ingrese los respectivos datos de la camisa.
     */
    public static void crearCamisa() throws IOException {
        out.println("Ingrese el código en números de la Montero.Lizbeth.bl.Camisa.");
        String id = (in.readLine());
        out.println("Ingrese el color  de la Montero.Lizbeth.bl.Camisa.");
        out.println("codigo de color: Rojo=1, Azul=02, Verde=00, Rosado=33.");
        String color = (in.readLine());
        out.println("Digite el tamaño deseado de la Montero.Lizbeth.bl.Camisa: xs,s,m,l,xl.");
        String tam = (in.readLine());
        out.println("Ingrese la descripción de su Montero.Lizbeth.bl.Camisa.");
        String descri = (in.readLine());
        out.println("Ingrese el precio de la Montero.Lizbeth.bl.Camisa.");
        String precio = (in.readLine());
        if (administrador.validarCamisa(tam, color, id, descri, precio) == null) {
            administrador.registrarCamisa(tam, color, id, descri, precio);
            out.println("La camisa se a registrado correctamente");
        } else {
            out.println("Ese dato ya existe, no se pueden repetir los registros de ningun elemento.");
        }
        crearMenu();
    }// FIN crearCamisa.
    /**
     * metodo en donde por medio del controller si llama a la funcion para imprimir la lista de camisas.
     */
    public static void imprimirCamisa() throws IOException {
        out.println("La lista de camisas es: \n");
        String[] lista = administrador.ListarCamisa();
        for (String info : lista) {
            out.println(info);
        }
        out.println("Fin\n");
        crearMenu();
    }// FIN imprimirCamisa.
    /**
     * metodo en donde se le pide al usuario que ingrese los respectivos datos del catalogo y se le muestra los mensajes del procedimiento al usuario.
     */
    public static void crearCatalogo() throws IOException {
        out.println("Ingrese el Id en números de su catálogo (codigo).");
        String Id = (in.readLine());
        out.println("Ingrese el nombre del mes de creación del catálogo.");
        String nombreDelMeS = (in.readLine());
        out.println("Ingrese el año de creación en números.");
        String year = (in.readLine());
        out.println("Ingrese la fecha de creación.");
        String fechaDeCreacion = (in.readLine());

        if (administrador.validarCatalogo(year, Id, fechaDeCreacion, nombreDelMeS) == null) {
            administrador.registrarCatalogo(year, Id, fechaDeCreacion, nombreDelMeS);
            out.println("El catálogo se a registrado correctamente");
        } else {
            out.println("Ese dato ya existe, no se pueden repetir los registros de ningun elemento.");
        }
        crearMenu();
    }// FIN
    /**
     * metodo en donde por medio del controller si llama a la funcion para imprimir la lista de catalogos.
     */
    public static void imprimirCatalogo() throws IOException {
        out.println("La lista de catálogos es: \n");
        String[] lista = administrador.ListarCatalogos();
        for (String info : lista) {
            out.println(info);
        }
        out.println("Fin\n");
        crearMenu();
    }// FIN imprimirCatalogo.
    /**
     * metodo en donde se le pide al usuario que ingrese los respectivos datos del cliente y se le muestra los mensajes del procedimiento al usuario.
     */
    static void crearCliente() throws IOException {
        out.println("Ingrese su nombre");
        String Nombre = (in.readLine());
        out.println("Ingrese su primer apellido.");
        String apellido1 = (in.readLine());
        out.println("Ingrese su segundo apellido.");
        String apellido2 = (in.readLine());
        out.println("Digite la dirección exacta donde desea resivir su catálogo.");
        String dirreccionExacta = (in.readLine());
        out.println("Ingrese su correo electronico.");
        String correo = (in.readLine());

        if (administrador.validarCliente(correo, apellido1, apellido2, dirreccionExacta, Nombre) == null) {
            administrador.registrarCliente(Nombre, apellido1, apellido2, dirreccionExacta, correo);
            out.println("El cliente se a registrado correctamente");
        } else {
            out.println("El correo que usted ingreso ya existe.");
        }
        crearMenu();
    }// FIN
    /**
     * metodo en donde por medio del controller si llama a la funcion para imprimir la lista de clientes.
     */
    public static void imprimirCliente() throws IOException {
        out.println("La lista de clientes registrados es: \n");
        String[] lista = administrador.ListarCliente();
        for (String info : lista) {
            out.println(info);
        }
        out.println("Fin\n");
        crearMenu();
    }// FIN imprimirCamisa.
/**
 *metodo en  donde se crea el menu.
 */
    public static void crearMenu() throws IOException {
        String opcionMenu;
        do {
            System.out.println("Escoja una opción: \n1. Crear catálogos \n2. Registar Clientes \n3.Registar camisas \n4. Listar Camisas \n5. Listar catálogos \n6. Listar clientes  \n7. Salir");
            opcionMenu = in.readLine();
            switch (opcionMenu) {
                case "1":
                    crearCatalogo();
                    break;
                case "2":
                    crearCliente();
                case "3":
                    crearCamisa();
                    break;
                case "4":
                    imprimirCamisa();
                    break;
                case "5":
                    imprimirCatalogo();
                    break;
                case "6":
                    imprimirCliente();
                case "7":
                    System.exit(0);
                    break;
            }
            out.print("");
        } while (opcionMenu != "7");

    }// FIN CM
}// FIN DE PROGRAMA

