package Montero.Lizbeth.bl;
/**
 * Clase Cliente, la cual es creada en el en la package o capa bl y aquí es en donde se guardan los valores de cada atributo del Cliente.
 * @version 1.0.1
 * @author  Lizbeth Montero Ramirez.
 * @since 1.0.0
 */
public class Cliente {
    //Atributos
    private String Nombre, apellido1, apellido2, dirreccionExacta, correo;
    /**Contructor por defecto de la calase Cliente,puede usarse para hacer casos de prueba o invocación de subclases.
     */
    public Cliente() {
    }// FIN DEL CONTRUCTOR POR DEFECTO.

    /**
     * Constructor que recibe todos los parámetros. evita que algún valor se almacene por defecto osea con el valor de "null" en Strings o "0" en enteros...
     * @param Nombre: variable de la clase Cliente de tipo String en el cual se guarda el nombre del cliente ingresado por el usuario.
     * @param apellido1: variable de la clase Cliente de tipo String en el cual se guarda el primer apellido del cliente ingresado por el usuario.
     * @param apellido2: variable de la clase Cliente de tipo String en el cual se guarda el segundo apellido  del cliente ingresado por el usuario.
     * @param dirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param correo: variable de la clase Cliente en donde se almacena el correo de la persona quien hace el pedido, es de tipo String.
     */
    public Cliente(String Nombre, String apellido1, String apellido2, String dirreccionExacta, String correo) {
        this.Nombre = Nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.dirreccionExacta = dirreccionExacta;
        this.correo = correo;
    }// FIN DE CONTRUCTOR QUE RECIBE PARAMETROS.

    /**
     * Metódo Gettter de la variable nombre, es utilizado para obterner el atributo privado de una clase.
     * @return devuelve la varible que almacena el nombre  del cliente (lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getNombre() {
        return Nombre;
    }
    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param nombre: dato que almacena el nombre del Cliente, ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setNombre(String nombre) {
        Nombre = nombre;
    }
    /**
     * Metódo Gettter de la variable primer apellido, es utilizado para obterner el atributo privado de una clase.
     * @return devuelve la varible que almacena el primer appelido (lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getApellido1() {
        return apellido1;
    }
    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param apellido1: dato que almacena el primer appelido  del Cliente, ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    /**
     * Metódo Gettter de la variable segundo apellido, es utilizado para obterner el atributo privado de una clase.
     * @return devuelve la varible que almacena el segundo appelido (lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getApellido2() {
        return apellido2;
    }
    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param apellido2: dato que almacena el segundo appelido  del Cliente, ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
    /**
     * Metódo Gettter de la variable segundo direcccion exacta, es utilizado para obterner el atributo privado de una clase.
     * @return devuelve la varible que almacena  la direcccion exacta (lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getDirreccionExacta() {
        return dirreccionExacta;
    }
    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param dirreccionExacta: dato que almacena direcccion exacta,  del Cliente, ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setDirreccionExacta(String dirreccionExacta) {
        this.dirreccionExacta = dirreccionExacta;
    }
    /**
     * Metódo Gettter de la variable correo, es utilizado para obterner el atributo privado de una clase.
     * @return devuelve la varible que almacena el correo (lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getCorreo() {
        return correo;
    }
    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param correo: dato que almacenael correo ,  del Cliente, ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    /**
     * Método toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase cliente en un solo String.
     */
    @Override
    public String toString() {
        return "Cliente{" +
                "Nombre='" + Nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", dirreccionExacta='" + dirreccionExacta + '\'' +
                ", correo='" + correo + '\'' +
                '}';
    }


}// FIN DE CLAs




