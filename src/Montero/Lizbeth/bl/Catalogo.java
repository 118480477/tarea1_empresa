package Montero.Lizbeth.bl;

/**
 * Clase Catálogo, la cual es creada en el en la package o capa bl y aquí es en donde se guardan los valores de cada atributo del catálogo.
 * @author  Lizbeth Montero Ramirez.
 * @version 1.0.1
 * @since 1.0.0
 */
public class Catalogo {
    //Atributos
    private String year, Id ;
    private String fechaDeCreacion, nombreDelMeS;
    /**
     * constructor por defecto de la clase catálogo que es utilizado para la invocación de las subclases.
     */
    public Catalogo() {
    }// FIN DE CONSTRUCTOR POR DEFECTO.

    /**
     * Constructor que recibe todos los parámetros, evita que algún valor se almacene por defecto osea con el valor de "null" en Strings o "0" en enteros..
     * @param year: dato de la clase catálogo de tipo String en el cual se guarda el año de creación de dicho catálogo ingresado por el usuario.
     * @param Id: dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param fechaDeCreacion: dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @param nombreDelMeS: variable en donde se almacena el nombre del mes de creación de dicho catálogo.
     */
    public Catalogo(String year, String Id, String fechaDeCreacion, String nombreDelMeS) {
        this.year=year;
        this.Id=Id;
        this.fechaDeCreacion=fechaDeCreacion;
        this.nombreDelMeS=nombreDelMeS;
    }// FIN DE CONSTRUCTOR QUE RECIBE LOS PARAMETROS.

    /**
     * Metódo Gettter de la variable que almacena el año de creación; es utilizado para obterner el atributo privado de una clase.
     * @return el dato que devuelve es el atributo de que almacena el año de creación del catálogo, ( lo vuleve público) y
     * de está manera puede ser nombrado en otras calses.
     */

    public String getYear() {
        return year;
    }

    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param year: dato que almacena el año de creación del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * Metódo Gettter de la variable que almacena el código identificativo del catalogo; es utilizado para obterner el atributo privado de una clase.
     * @return retorna del atributo de que almacena Id del catálogo, ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getId() {
        return Id;
    }

    /**
     * Metódo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al código del catálogo.
     * @param id: variable que almacena el código del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setId(String id) {
        Id = id;
    }

    /**
     * Metódo Gettter de la variable que almacena la fecha de creación del catalogo; es utilizado para obterner el atributo privado de un objeto en su respectiva  clase.
     * @return retorna la variable que almacena la fecha de creación del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public String getfechadecreacion() {
        return fechaDeCreacion;
    }

    /**
     * Metódo Set es utilizado para asignar valores a los atributos privados de una clase, en este caso a la variable de la fecha de creación del catálogo.
     * @param fechaDeCreacion: variable que almacena la fecha de creación del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    /**
     * Metódo get de la variable que guarda el el nombre del mes en el que fue creado el catálogo. Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     * @return devuelve la variable que guarda el nombre del mes de creación del catálogo el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */

    public String getNombreDelMeS() {
        return nombreDelMeS;
    }

    /**
     * Metódo setes utilizado para asignar valores a los atributos privados de una clase, en este caso a la variable de la fecha de creación del catálogo.
     * @param nombreDelMeS:  variable que guarda el nombre del mes de la creación del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setNombreDelMeS(String nombreDelMeS) {
        this.nombreDelMeS = nombreDelMeS;
    }

    /**
     * Método toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo del catálogo en un solo String.
     */
    @Override
    public String toString() {
        return "Catalogo{" +
                "year=" + year +
                ", Id=" + Id +
                ", fechaDeCreación='" + fechaDeCreacion + '\'' +
                ", nombreDelMeS='" + nombreDelMeS + '\'' +
                '}';
    }
}// FIN DE PROGRAMA.