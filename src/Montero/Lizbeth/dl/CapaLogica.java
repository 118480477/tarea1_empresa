package Montero.Lizbeth.dl;

import Montero.Lizbeth.bl.Catalogo;
import Montero.Lizbeth.bl.Camisa;
import Montero.Lizbeth.bl.Cliente;

import java.util.ArrayList;

/**
 * Clase Capa Logica, la cual es creada en el en la package o capa dl y aquí es en donde se programa la logica de negocio.
 *
 * @version 1.0.1
 * @author  Lizbeth Montero Ramírez.
 * @since 1.0.0
 */
public class CapaLogica {
    private ArrayList<Catalogo> catalogos;
    private ArrayList<Camisa> camisas;
    private ArrayList<Cliente> clientes;

    /**
     * Contructor que es utilizado para inicializar los arreglos.
     */
    public CapaLogica() {
        catalogos = new ArrayList<>();
        camisas = new ArrayList<>();
        clientes = new ArrayList<>();
    }// FIN DE CONTRUCTOR.

    /**
     * Metodo para agregar el objeto al ArreyList del catálogo.
     *
     * @param ct parametro del objeto catalogo.
     */
    public void agregarCatalogo(Catalogo ct) {
        catalogos.add(ct);
    }
    /**
     * Metodo para agregar el objeto al ArreyList del camisa.
     *
     * @param ca parametro del objeto camisa.
     */
    public void agregarCamisa(Camisa ca) {
        camisas.add(ca);
    }

    /**
     * Metodo para agregar el objeto al ArreyList del camisa.
     *
     * @param cl arametro del objeto cliente.
     */
    public void agregarCliente(Cliente cl) {
        clientes.add(cl);
    }

    /**
     * Metodo que recorre el nuevo arreglo de catalogos.
     *
     * @return devuelve la información del arreglo de String.
     */
    public String[] getCatalogo() {
        int cont = 0;
        String[] info = new String[catalogos.size()];
        for (Catalogo resgisCat : catalogos) {
            info[cont] = resgisCat.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCATALOGO.

    /**
     * metodo utilizado para validar la  información del catalogo, para que nada se repita.
     * @param year: dato de la clase catálogo de tipo String en el cual se valida el año de creación de dicho catálogo ingresado por el usuario.
     * @param Id:dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param fechaDeCreacion: dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @param nombreDelMeS:variable en donde se valida el nombre del mes de creación de dicho catálogo.
     * @return retorna si un dato se repite o no.
     */
    public String validarCatalogo(String year, String Id, String fechaDeCreacion, String nombreDelMeS) {
        for (Catalogo cat : catalogos) {
            if (year.equals(cat.getYear()) || Id.equals(cat.getId()) || fechaDeCreacion.equals(cat.getfechadecreacion()) || nombreDelMeS.equals(cat.getNombreDelMeS())) {
                return cat.getYear();
            } else if (cat.equals(cat.getId())) {
                return cat.getId();
            } else if (cat.equals(cat.getfechadecreacion())) {
                return cat.getfechadecreacion();
            } else if (cat.equals(cat.getNombreDelMeS())) {
                return cat.getNombreDelMeS();
            }
        }
        return null;
    }//FIN DE VALIDAR CATALOGO.

    /**
     * Metodo que recorre el nuevo arreglo de camisa.
     *
     * @return devuelve la información de el arreglo getCamisa de tipo  arreglo de String.
     */
    public String[] getCamisa() {
        int cont = 0;
        String[] info = new String[camisas.size()];
        for (Camisa rCamisa : camisas) {
            info[cont] = rCamisa.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCAMISA.
    /**
     * metodo utilizado para validar la  información de la camisa, para que nada se repita.
     * @param tam:    dato de la clase Camisa de tipo String en el cual se valida el tamaño de la creación de la camisa ingresado por el usuario.
     * @param color:  variable en donde se valida el color de la camisa deseada por el cliente.
     * @param id:dato identificativo de la camisa, hace referencia a un codigo único de la mismo.
     * @param descri: variable que describe detalladamente las caracteristicas de la camisa.
     * @param precio: variable que contiene el precio despectivo de la camisa en especifico ingresado por el usuario
     * @return retorna si un dato se repite o no.
     */
    public String validarCamisa(String tam, String color, String id, String descri, String precio) {
        for (Camisa Rcam : camisas) {
            if (tam.equals(Rcam.getTam()) || color.equals(Rcam.getColor()) || id.equals(Rcam.getid()) || descri.equals(Rcam.getDescri()) || precio.equals(Rcam.getPrecio())) {
                return Rcam.getTam();
            } else if (color.equals(Rcam.getColor())) {
                return Rcam.getColor();
            } else if (id.equals(Rcam.getid())) {
                return Rcam.getid();
            } else if (descri.equals(Rcam.getDescri())) {
                return Rcam.getDescri();
            } else if (precio.equals(Rcam.getPrecio())) {
                return Rcam.getPrecio();
            }
        }
        return null;
    }//Fin

    /**
     * Metodo que recorre el nuevo arreglo de cliente.
     *
     * @return devuelve la información de el arreglo getCliente  de tipo  arreglo de String.
     */
    public String[] getCliente() {
        int cont = 0;
        String[] info = new String[clientes.size()];
        for (Cliente rCliente : clientes) {
            info[cont] = rCliente.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCLIENTE.

    /**
     *metodo utilizado para validar la  información del cliente, para que nada se repita.
     * @param Pnombre: variable de la clase Cliente de tipo String en el cual se valida el nombre del cliente ingresado por el usuario.
     * @param Papellido1: variable de la clase Cliente de tipo String en el cual se valida el primer apellido del cliente ingresado por el usuario.
     * @param Papellido2: variable de la clase Cliente de tipo String en el cual se valida el segundo apellido  del cliente ingresado por el usuario.
     * @param PdirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param Pcorreo: variable de la clase Cliente en donde se valida el correo de la persona quien hace el pedido, es de tipo String.
     * @return retorna si un dato se repite o no.
     */
    public String validarCliente(String Pnombre, String Papellido1, String Papellido2, String PdirreccionExacta, String Pcorreo) {

        for (Cliente rCliente : clientes) {
            if (Pcorreo.equals(rCliente.getCorreo()) || Papellido1.equals(rCliente.getApellido1()) || Papellido2.equals(rCliente.getApellido2()) || PdirreccionExacta.equals(rCliente.getDirreccionExacta()) || Pnombre.equals(rCliente.getNombre())) {
                return rCliente.getCorreo();
            } else if (Papellido1.equals(rCliente.getApellido1())) {
                return rCliente.getApellido1();
            } else if (Papellido2.equals(rCliente.getApellido2())) {
                return rCliente.getApellido2();
            } else if (PdirreccionExacta.equals(rCliente.getDirreccionExacta())) {
                return rCliente.getDirreccionExacta();
            } else if (Pnombre.equals(rCliente.getNombre())) ;
            return rCliente.getNombre();
        }//Fin
        return null;
    }
}// FIN DE CLASS.
