package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.Catalogo;
import Montero.Lizbeth.bl.Camisa;
import Montero.Lizbeth.bl.Cliente;
import Montero.Lizbeth.dl.CapaLogica;

import java.util.ArrayList;

/**
 * Clase Controller, la cual es creada en el en la package o capa tl este es el administrador, se encarga de nombrar y ejecutar todos los metodos.
 *
 * @version 1.0.1
 * @author  Lizbeth Montero Ramirez.
 * @since 1.0.0
 */
public class Controller {
    private CapaLogica logica;
    /**
     * Constructor de la capa logica, se inicializa la acapa logica para que esta pueda ser sub llamada.
     */
    public Controller() {
        logica = new CapaLogica();
    }

    /**
     * meteodo en donde se incializa el objeto catalogo y se manda a que se almacene en el ArrayList, llamando la capa logica
     *
     * @param year:            dato de la clase catálogo de tipo String en el cual se guarda el año de creación de dicho catálogo ingresado por el usuario.
     * @param Id:              dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param fechaDeCreacion: dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @param nombreDelMeS:    variable en donde se almacena el nombre del mes de creación de dicho catálogo.
     */

    public void registrarCatalogo(String year, String Id, String fechaDeCreacion, String nombreDelMeS) {
        Catalogo ct = new Catalogo(year, Id, fechaDeCreacion, nombreDelMeS);
        logica.agregarCatalogo(ct);
    }// FIN DE REGISTAR CATALOGO

    /**
     * metodo usado para validar el catalogo llamando la funcion que está en la capa logica.
     *
     * @param year:            dato de la clase catálogo de tipo String en el cual se guarda el año de creación de dicho catálogo ingresado por el usuario.
     * @param Id:              dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param fechaDeCreacion: dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @param nombreDelMeS:    variable en donde se almacena el nombre del mes de creación de dicho catálogo.
     * @return retorna la funcion de capa logica donde calcula si se repite o no.
     */

    public String validarCatalogo(String year, String Id, String fechaDeCreacion, String nombreDelMeS) {
        return logica.validarCatalogo(year, Id, fechaDeCreacion, nombreDelMeS);

    }//FIN DE VALIDAR CATALOGO.

    /**
     * metodod usado para imprimir el arrayList de catalogo.
     *
     * @return retorna la lista de catalogos.
     */
    public String[] ListarCatalogos() {
        return logica.getCatalogo();
    }// FIN DE LISTAR CATALOGO.

    /**
     * meteodo en donde se incializa el objeto camisa y se manda a que se almacene en el ArrayList, llamando la capa logica
     *
     * @param tam:    dato de la clase Camisa de tipo String en el cual se guara el tamaño de la creación de la camisa ingresado por el usuario.
     * @param color:  variable en donde se guarda el color de la camisa deseada por el cliente.
     * @param id:dato identificativo de la camisa, hace referencia a un codigo único de la mismo.
     * @param descri: variable que describe detalladamente las caracteristicas de la camisa.
     * @param precio: variable que contiene el precio despectivo de la camisa en especifico ingresado por el usuario
     */
    public void registrarCamisa(String tam, String color, String id, String descri, String precio) {
        Camisa ca = new Camisa(tam, color, id, descri, precio);
        logica.agregarCamisa(ca);
    }// FIN DE REGISTAR CAMISA.
    /**
     * metodo usado para validar la camisa  llamando la funcion que está en la capa logica.
     * @param tam:    dato de la clase Camisa de tipo String en el cual se guara el tamaño de la creación de la camisa ingresado por el usuario.
     * @param color:  variable en donde se guarda el color de la camisa deseada por el cliente.
     * @param id:dato identificativo de la camisa, hace referencia a un codigo único de la mismo.
     * @param descri: variable que describe detalladamente las caracteristicas de la camisa.
     * @param precio: variable que contiene el precio despectivo de la camisa en especifico ingresado por el usuario
     * @return retorna la funcion de capa logica donde calcula si se repite o no.
     */
    public String validarCamisa(String tam, String color, String id, String descri, String precio) {
        return logica.validarCamisa(tam, color, id, descri, precio);

    }// FIN VALIDARCLIENTE

    /**
     * metodod usado para imprimir el arrayList de camisa.
     *
     * @return retorna la lista de camisas.
     */
    public String[] ListarCamisa() {
        return logica.getCamisa();
    }// FIN DE LISTAR CAMISA.

    /**
     * meteodo en donde se incializa el objeto cliente y se manda a que se almacene en el ArrayList, llamando la capa logica
     *
     * @param Nombre:               variable de la clase Cliente de tipo String en el cual se guarda el nombre del cliente ingresado por el usuario.
     * @param apellido1:            variable de la clase Cliente de tipo String en el cual se guarda el primer apellido del cliente ingresado por el usuario.
     * @param apellido2:            variable de la clase Cliente de tipo String en el cual se guarda el segundo apellido  del cliente ingresado por el usuario.
     * @param dirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param correo:               variable de la clase Cliente en donde se almacena el correo de la persona quien hace el pedido, es de tipo String.
     **/
    public void registrarCliente(String Nombre, String apellido1, String apellido2, String dirreccionExacta, String correo) {
        Cliente cl = new Cliente(Nombre, apellido1, apellido2, dirreccionExacta, correo);
        logica.agregarCliente(cl);

    }//FIN REGISTRAR CLIENTE.

    /**
     * metodo usado para validar el cliente llamando la funcion que está en la capa logica.
     *
     * @param Pnombre: variable de la clase Cliente de tipo String en el cual se guarda el nombre del cliente ingresado por el usuario.
     * @param Papellido1: variable de la clase Cliente de tipo String en el cual se guarda el primer apellido del cliente ingresado por el usuario.
     * @param Papellido2: variable de la clase Cliente de tipo String en el cual se guarda el segundo apellido  del cliente ingresado por el usuario.
     * @param PdirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param pcorreo: variable de la clase Cliente en donde se almacena el correo de la persona quien hace el pedido, es de tipo String.
     * @return retorna la funcion de capa logica donde calcula si se repite o no.
     */
    public String validarCliente(String pcorreo, String Papellido1, String Papellido2, String PdirreccionExacta, String Pnombre) {
        return logica.validarCliente(pcorreo, Papellido1, Papellido2, PdirreccionExacta, Pnombre);
    }

    /**
     * metodod usado para imprimir el arrayList de cliente.
     *
     * @return retorna la lista de clientes.
     */
    public String[] ListarCliente() {
        return logica.getCliente();

    }// FIN DE LISTAR CLIENTE.
}// FIN DE  CLASS.
